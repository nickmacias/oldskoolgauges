using Toybox.WatchUi;
using Toybox.Graphics;
using Toybox.Sensor;

//
// cadence/speed/distance based loosly on my Hyundai dash :)
// Nick Macias July 2020
// Released under Creative Commons Attribution International 4.0
// Feel free to use/modify/re-distribute/sell/whatever, provided
// you retain this attribution and license
//
// No warranty here! This should work, but use it only in non-critical situations.
//

class OldSkoolGaugesView extends WatchUi.DataField {
// made for Forerunner 920XT, may also work on VivoActive
// (only made for square displays similar in size to the Forerunner)

  var cadence=0;
  var speed=0;
  var distance=0; // update these in the compute() method
  var dialLabel; // evil global (too many args?!?)

  function initialize() { // kinda boring...
    DataField.initialize();
  }

  function compute(info) { // called when there is new data available
    if (info == null){
      return;
    } // something weird...
    
// read cadence
    if (info.currentCadence != null){
      cadence=info.currentCadence;
      if (cadence > 125){
        cadence=125; // cap it here
      }
    }
    cadence=cadence.toFloat(); // so we can divide etc.

// read speed and distance
    if (info.currentSpeed != null){
      speed=info.currentSpeed*2.23694; // from meters per sec to mph
    }
    if (info.elapsedDistance != null){
      distance=info.elapsedDistance*.06214; // from meters to hundredths of a mile
    }
    distance=distance.toLong();
    //System.println("c=" + cadence + " s=" + speed + " d=" + distance);
  }

    // Display the value you computed here. This will be called
    // once a second when the data field is visible.
  function onUpdate(dc) {
    var width=dc.getWidth();
    var height=dc.getHeight();
    //var backgroundColor=getBackgroundColor();

// odometer
    drawNum(dc,width/2 - 35,-9+height*4/5,19,34,(distance/10000)%10,false); // hundreds
    drawNum(dc,width/2 - 15,-9+height*4/5,19,34,(distance/1000)%10,false); // tens
    drawNum(dc,width/2 +  5,-9+height*4/5,19,34,(distance/100)%10,false); // units
    drawNum(dc,width/2 + 25,-9+height*4/5,19,34,distance%100,true);  // tenths (pass hundredths though)

// tachymeter and speed
    dialLabel="RPM x10"; // problem passing label ("too many arguments" error)
    drawMeter(dc,cadence.toFloat()/10,width/4,35+height/4,height/3,13,1,3,9);
    dialLabel="MPH";
    drawMeter(dc,speed,3*width/4,35+height/4,height/3,7,5,2,5);
  }
    
// draw one digit of odometer
// (x1,y1) are coordinates of lower left corder
// w, h are width and height of rectangle
// val is digit to draw (two digits if last digit)
// reverse is TRUE for last digit (reverse video)
  function drawNum(dc,x1,y1,w,h,val,reverse)
  {
    dc.setPenWidth(1);

// draw background rectangle first
    dc.setColor(reverse?Graphics.COLOR_WHITE:Graphics.COLOR_BLACK,Graphics.COLOR_TRANSPARENT); // normally black
    dc.fillRectangle(x1,y1,w,h);

// now draw the digit
    dc.setColor(reverse?Graphics.COLOR_BLACK:Graphics.COLOR_WHITE,Graphics.COLOR_TRANSPARENT); // normally white
// for the tenths digit, let's scroll the wheel
    if (!reverse){ // normal
      dc.drawText(x1+10,y1+4,Graphics.FONT_SYSTEM_LARGE,""+val,Graphics.TEXT_JUSTIFY_CENTER);
    } else { // scroll it!
// find the value and how much to scroll
      var val2=val%10; // hundredths (0-9)
      val=val/10; // tenths (0-9)
// draw current tenths digit, offset by hundredths
      var newy=y1-(h.toFloat()*(val2.toFloat()/10));
      dc.drawText(x1+10,newy+4,Graphics.FONT_SYSTEM_LARGE,""+val,Graphics.TEXT_JUSTIFY_CENTER); // current digit rolls off the top
// draw next digit
      newy=newy+h;
      dc.drawText(x1+10,newy+4,Graphics.FONT_SYSTEM_LARGE,""+((val+1)%10),Graphics.TEXT_JUSTIFY_CENTER); // next digit comes from the bottom

// wipe out the excess
      dc.setColor(Graphics.COLOR_WHITE,Graphics.COLOR_TRANSPARENT); // this is only happending on tenths (reverse=TRUE)
      dc.fillRectangle(x1,y1-h,w,h); // overwrite above the box
      dc.fillRectangle(x1,y1+h,w,h); // and below
    } // done with special handling

// now draw outline of the box
    dc.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_TRANSPARENT);
    dc.drawRectangle(x1,y1,w,h);
  }
    
// draw a meter
// value is the value for the needle
// (xc,yc) is the center of the dial
// rad is the radius of the dial
// numTics is the total number of tic marks to display
// valPerTic is how much each tic represents
// ticPerLabel says how often the tics are labeled
// criticalTic is for non-MPH dial, and says when to color-code RED
  function drawMeter(dc,value,xc,yc,rad,numTics,valPerTic,ticPerLabel,criticalTic)
  {
// outer arc
    dc.setPenWidth(3);
    if (dialLabel.equals("MPH")){
// 3 color ranges
      dc.setColor(Graphics.COLOR_GREEN,Graphics.COLOR_TRANSPARENT);
      dc.drawArc(xc,yc,rad,Graphics.ARC_CLOCKWISE,135,105);

      dc.setColor(Graphics.COLOR_ORANGE,Graphics.COLOR_TRANSPARENT);
      dc.drawArc(xc,yc,rad,Graphics.ARC_CLOCKWISE,105,75);
    
      dc.setColor(Graphics.COLOR_RED,Graphics.COLOR_TRANSPARENT);
      dc.drawArc(xc,yc,rad,Graphics.ARC_CLOCKWISE,75,45);
    } else { // rpm
      dc.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_TRANSPARENT);
      dc.drawArc(xc,yc,rad,Graphics.ARC_CLOCKWISE,135,45);
    
// high-value arc for RPM
      dc.setColor(Graphics.COLOR_RED,Graphics.COLOR_TRANSPARENT);
      dc.drawArc(xc,yc,rad,Graphics.ARC_CLOCKWISE,68,45);
    }

// draw tics
    dc.setPenWidth(1);
    dc.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_TRANSPARENT);
    var angle=-2.35; // first tics (angles are negative since Y runs from 0 at top to positive at bottom)

    var label;
    var len;
    
    for (var tic=0;tic<numTics;tic++){
      if ((tic%ticPerLabel) == 0){ // add a label
        label="" + (tic*valPerTic);
        len=11; // longer line
      } else {
        label="";
        len=8; // short line
      }
      
      if (dialLabel.equals("MPH")){ // set color bands differently here
        if (tic==0){
          dc.setColor(Graphics.COLOR_GREEN,Graphics.COLOR_TRANSPARENT);
        } // persists until next change :)
        if (tic==2){
          dc.setColor(Graphics.COLOR_ORANGE,Graphics.COLOR_TRANSPARENT);
        }
        if (tic==4){
          dc.setColor(Graphics.COLOR_RED,Graphics.COLOR_TRANSPARENT);
        }
      }
        
      if (tic==criticalTic){ // last few are in red!
        dc.setColor(Graphics.COLOR_RED,Graphics.COLOR_TRANSPARENT);
      }

// now draw the tick mark at the chosen length and with the chosen label
      tic(dc,xc,yc,rad,angle,len,label);
      angle+=1.5708/(numTics-1); // radians in one tic-to-tic arc, = (pi/2)/(# of tics - 1)
    } // end of tic drawing loop

// draw the gauge's label
    dc.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_TRANSPARENT);
    dc.drawText(xc-7,yc+5,Graphics.FONT_TINY,dialLabel,Graphics.TEXT_JUSTIFY_CENTER);
      
// draw needle
    dc.setPenWidth(4);
    angle=135-(value/(valPerTic*(numTics-1)))*90; // 0=125 deg, 120=45 deg
    angle=-angle*0.017453293; // convert to radians and flip for Y
    dc.drawLine(xc,yc,xc+rad*Math.cos(angle),yc+rad*Math.sin(angle));
  }

// draw a single tic mark
// (xc,yc) is the center of the dial
// r is the radius
// angle is the angle :)
// len is how long the tic should be (towards the center)
// label is the label for the tic (or "" if no label)
  function tic(dc,xc,yc,r,angle,len,label)
  {
    var c=Math.cos(angle);
    var s=Math.sin(angle);
    var x1=xc+r*c;
    var y1=yc+r*s;
    var x2=xc+(r-len)*c;
    var y2=yc+(r-len)*s;
    dc.drawLine(x1,y1,x2,y2);
    if (!label.equals("")){
      dc.drawText(x1,y1-19,Graphics.FONT_XTINY,label,Graphics.TEXT_JUSTIFY_CENTER);
    }
  }
}