Wanted to try making an IQ Connect Complex Datafield, so made a gauge loosely based on my Hyundai's dashboard. Probably not the most practical instrumentation -- did it more for fun and learning -- but I like how it came out so thought I'd share it.
The tachometer goes from 0-12 (x10 rpm), red-lines at 90 (cuz, why not!)
The speedometer from 0-30 mph (only), with 3 zones (0-10=green; 10-20=orange; 20-30=red)
Odometer goes up to 999.9 miles, current trip only. Tenths digit scrolls; others snap.

I made this for the Forerunner 920XT, but it looks like it probably works on the Vivoactive as well.

Video demo/discussion here: https://youtu.be/PIrMkcR1PBQ

Video of improved dial scroll here: https://youtu.be/GS_9BCwqCW8

Have fun!